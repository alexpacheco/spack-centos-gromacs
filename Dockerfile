# Build stage with Spack pre-installed and ready to be used
FROM alexpacheco/spack-centos-gcc:7.831 as builder

# What we want to install and how we want to install it
# is specified in a manifest file (spack.yaml)
RUN mkdir /opt/spack-environment \
&&  (echo "spack:" \
&&   echo "  specs:" \
&&   echo "  - gromacs@2020.2 ~hwloc ^mpich" \
&&   echo "  concretization: together" \
&&   echo "  config:" \
&&   echo "    install_tree: /opt/software" \
&&   echo "  view: /opt/view") > /opt/spack-environment/spack.yaml

RUN sed -i 's/x86_64/sandybridge/g' /opt/spack/etc/spack/packages.yaml

# Install the software, remove unecessary deps
RUN cd /opt/spack-environment && \
    spack env activate -d . && spack install && spack gc -y && spack env deactivate

# Strip all the binaries
RUN find -L /opt/view/* -type f -exec readlink -f '{}' \; | \
    xargs file -i | \
    grep 'charset=binary' | \
    grep 'x-executable\|x-archive\|x-sharedlib' | \
    awk -F: '{print $1}' | xargs strip -s

# Modifications to the environment that are necessary to run
RUN cd /opt/spack-environment && \
    spack env activate --sh -d . >> /etc/profile.d/z10_spack_environment.sh

# Bare OS image to run the installed executables
FROM centos:7

COPY --from=builder /opt/spack-environment /opt/spack-environment
COPY --from=builder /opt/software /opt/software
COPY --from=builder /opt/view /opt/view
COPY --from=builder /etc/profile.d/z20_devtools8.sh /etc/profile.d/z20_devtools8.sh
COPY --from=builder /etc/profile.d/z10_spack_environment.sh /etc/profile.d/z10_spack_environment.sh

# Install gcc 8.x from devtools
RUN yum update -y \
 && yum install -y centos-release-scl \
 && yum -y --enablerepo=centos-sclo-rh-testing install devtoolset-8-gcc devtoolset-8-gcc-c++ devtoolset-8-gcc-gfortran \
 && rm -rf /var/cache/yum \
 && yum clean all 

RUN echo 'export PS1="\[$(tput bold)\]\[$(tput setaf 1)\][gromacs]\[$(tput setaf 2)\]\u\[$(tput sgr0)\]:\w $ \[$(tput sgr0)\]"' >> ~/.bashrc
ENV PATH=/opt/view/bin:$PATH
ENV LD_LIBRARY_PATH=/opt/view/lib64:/opt/view/lib:$LD_LIBRARY_PATH
ENV LD_INCLUDE_PATH=/opt/view/include:$LD_INCLUDE_PATH
ENV MANPATH=/opt/share/man:$MANPATH
WORKDIR /data

LABEL "app"="gromacs"
LABEL "mpi"="mpich"

#ENTRYPOINT ["gmx_mpi"]
#CMD ["-h"]

